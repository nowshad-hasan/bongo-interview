import org.junit.Assert;
import org.junit.jupiter.api.Test;

class InterviewTest {

    Car car = new Car();
    VehicleBuilder carFromBuilder = new VehicleBuilder.Builder().
            numOfWheels(Car.NUM_OF_WHEELS_IN_CAR).numOfPassengers(Car.NUM_OF_PASSENGERS_IN_CAR).hasGas(true).build();

    VehicleBuilder planeFromBuilder = new VehicleBuilder.Builder().
            numOfWheels(Plane.NUM_OF_WHEELS_IN_PLANE).numOfPassengers(Plane.NUM_OF_PASSENGERS_IN_PLANE).hasGas(true).build();

    @Test
    void isAnagram() {
        Assert.assertFalse(Anagram.isAnagram("eat", "tar"));
    }

    @Test
    void isAnagram2() {
        Assert.assertTrue(Anagram.isAnagram("bleat", "table"));
    }

    @Test
    void numberOfPassengersInCar() {
        Assert.assertEquals(Car.NUM_OF_PASSENGERS_IN_CAR, car.set_num_of_passengers());
    }

    @Test
    void numOfWheelsInCar() {
        Assert.assertEquals(Car.NUM_OF_WHEELS_IN_CAR, car.set_num_of_wheels());
    }

    @Test
    void hasGasInCar() {
        Assert.assertEquals(true, car.has_gas());
    }

    @Test
    void wheelsNumInCarBuilder() {
        Assert.assertEquals(Car.NUM_OF_WHEELS_IN_CAR, carFromBuilder.getNumOfWheels());
    }

    @Test
    void passengersNumInCarBuilder() {
        Assert.assertEquals(Car.NUM_OF_PASSENGERS_IN_CAR, carFromBuilder.getNumOfPassengers());
    }

    @Test
    void hasGasInCarBuilder() {
        Assert.assertEquals(true, carFromBuilder.hasGas());
    }

    @Test
    void wheelsNumInPlaneBuilder() {
        Assert.assertEquals(Plane.NUM_OF_WHEELS_IN_PLANE, planeFromBuilder.getNumOfWheels());
    }

    @Test
    void passengersInPlaneBuilder() {
        Assert.assertEquals(Plane.NUM_OF_PASSENGERS_IN_PLANE, planeFromBuilder.getNumOfPassengers());
    }

    @Test
    void hasGasInPlaneBuilder() {
        Assert.assertEquals(true, planeFromBuilder.hasGas());
    }
}
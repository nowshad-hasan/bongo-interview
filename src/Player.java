public interface Player {
    void play();

    void forward();

    void rewind();
}

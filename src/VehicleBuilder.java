public class VehicleBuilder {

// This is the alternate design pattern - builder pattern I used for making Car and Plane class.

    public static class Builder {
        private int numOfWheels;
        private int numOfPassengers;
        private boolean hasGas;


        public Builder numOfWheels(int numOfWheels) {
            this.numOfWheels = numOfWheels;
            return this;
        }

        public Builder numOfPassengers(int numOfPassengers) {
            this.numOfPassengers = numOfPassengers;
            return this;
        }

        public Builder hasGas(boolean hasGas) {
            this.hasGas = hasGas;
            return this;
        }

        public VehicleBuilder build() {
            VehicleBuilder vehicleBuilder = new VehicleBuilder();
            vehicleBuilder.numOfWheels = this.numOfWheels;
            vehicleBuilder.numOfPassengers = this.numOfPassengers;
            vehicleBuilder.hasGas = this.hasGas;

            return vehicleBuilder;
        }

    }

    private int numOfWheels;
    private int numOfPassengers;
    private boolean hasGas;

    public int getNumOfWheels() {
        return numOfWheels;
    }

    public int getNumOfPassengers() {
        return numOfPassengers;
    }

    public boolean hasGas() {
        return hasGas;
    }

    private VehicleBuilder() {

    }

}

public class Interview {
    public static void main(String[] args) {
        System.out.println(Anagram.isAnagram("bleat", "table"));

        Car car = new Car();
        Plane plane = new Plane();


        VehicleBuilder carFromBuilder = new VehicleBuilder.Builder().
                numOfWheels(Car.NUM_OF_WHEELS_IN_CAR).numOfPassengers(Car.NUM_OF_PASSENGERS_IN_CAR).hasGas(true).build();  //assuming this car runs on gas

        VehicleBuilder planeFromBuilder = new VehicleBuilder.Builder().
                numOfWheels(Plane.NUM_OF_WHEELS_IN_PLANE).numOfPassengers(Plane.NUM_OF_PASSENGERS_IN_PLANE).hasGas(true).build();

    }
}

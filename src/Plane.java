public class Plane implements Vehicle {

    public static int NUM_OF_WHEELS_IN_PLANE = 6;
    public static int NUM_OF_PASSENGERS_IN_PLANE = 375;

    @Override
    public int set_num_of_wheels() {
        return NUM_OF_WHEELS_IN_PLANE;
    }

    @Override
    public int set_num_of_passengers() {
        return NUM_OF_PASSENGERS_IN_PLANE;
    }

    @Override
    public boolean has_gas() {
        return true;
    }
}

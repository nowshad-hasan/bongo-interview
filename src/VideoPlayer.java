public class VideoPlayer implements Player {

    // Adding three listeners for play, forward and rewind buttons

    @Override
    public void play() {
        if (!isVideoRunning())
            playVideo();
        else
            pauseVideo();
        /*
        If Video not_running{
        play video;
        }
         */
    }

    @Override
    public void forward() {
        /*
         assuming video forward for 5 seconds by default.
         time = currentTime+5s
         seekingVideo(time);


         */
    }

    @Override
    public void rewind() {
        /*
         assuming video rewind for 5 seconds by default.
         time = currentTime-5s
         seekingVideo(time);

         */
    }

    boolean isVideoRunning() {
        return true; // checks if video running or not
    }

    void playVideo() {
        /*
         load file from internal storage and start playing it
         If already file is loaded then just start it
         mPlayer.start();

         */
    }

    void pauseVideo() {
        //mPlayer.pause();
    }

    void seekingVideo(int time) {
        // Place video to that time.
    }

}

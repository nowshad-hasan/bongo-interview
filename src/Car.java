public class Car implements Vehicle {

    public static int NUM_OF_WHEELS_IN_CAR = 7;
    public static int NUM_OF_PASSENGERS_IN_CAR = 5;



    @Override
    public int set_num_of_wheels() {
        return NUM_OF_WHEELS_IN_CAR;
    }

    @Override
    public int set_num_of_passengers() {
        return NUM_OF_PASSENGERS_IN_CAR;
    }

    @Override
    public boolean has_gas() {
        return true;  //assuming this car runs on gas
    }

}

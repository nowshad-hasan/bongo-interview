import java.util.HashMap;

public class Anagram {

    private static HashMap<Character, Integer> mMap1 = new HashMap<>();
    private static HashMap<Character, Integer> mMap2 = new HashMap<>();

    public static boolean isAnagram(String text1, String text2) {
        if (text1.length() != text2.length())
            return false;
        else if (text1.equals(text2))
            return true;
        else {
            for (int index = 0; index < text1.length(); ++index) {
                // this block for making map1
                if (mMap1.containsKey(text1.charAt(index)))
                    mMap1.put(text1.charAt(index), mMap1.get(text1.charAt(index)) + 1);
                else
                    mMap1.put(text1.charAt(index), 1);

                // this block for making map2

                if (mMap2.containsKey(text2.charAt(index)))
                    mMap2.put(text2.charAt(index), mMap2.get(text2.charAt(index)) + 1);
                else
                    mMap2.put(text2.charAt(index), 1);
            }
            return mMap1.equals(mMap2);
        }
    }
}
